package com.huidao.pojo;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

public class Students {
    
    private Long stuId;

    @NotEmpty(message="学生名不能为空")
    private String name;
	@Range(min=0,max=100,message="年龄不合法")
    private Integer age;

    private Long classId;
    
    private Classes classes;

    
    public Long getStuId() {
        return stuId;
    }

    
    public void setStuId(Long stuId) {
        this.stuId = stuId;
    }

    
    public String getName() {
        return name;
    }

    
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

   
    public Integer getAge() {
        return age;
    }

   
    public void setAge(Integer age) {
        this.age = age;
    }


	public Classes getClasses() {
		return classes;
	}


	public void setClasses(Classes classes) {
		this.classes = classes;
	}


	public Long getclassId() {
		return classId;
	}


	public void setclassId(Long classId) {
		this.classId = classId;
	}
	
}