package com.huidao.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.huidao.mapper.StudentsMapper;
import com.huidao.pojo.Students;
@Service
public class StudentServiceImp implements StudentsService {
	@Autowired
	private StudentsMapper studentsMapper;
	
	public void setStudentsMapper(StudentsMapper studentsMapper) {
		this.studentsMapper = studentsMapper;
	}

	@Override
	public boolean deleteByPrimaryKey(Long stuId) {
		// TODO Auto-generated method stub
		return studentsMapper.deleteByPrimaryKey(stuId)>0;
	}

	@Override
	public boolean insert(Students record) {
		// TODO Auto-generated method stub
		return studentsMapper.insert(record)>0;
	}

	@Override
	public Students selectByPrimaryKey(Long stuId) {
		// TODO Auto-generated method stub
		return studentsMapper.selectByPrimaryKey(stuId);
	}

	@Override
	public boolean updateByPrimaryKey(Students record) {
		// TODO Auto-generated method stub
		return studentsMapper.updateByPrimaryKey(record)>0;
	}

	@Override
	public List<Students> selectAllStudents() {
		// TODO Auto-generated method stub
		return studentsMapper.selectAllStudents();
	}

	@Override
	public List<Students> selectStudentsByclassId(Long id) {
		// TODO Auto-generated method stub
		return studentsMapper.selectStudentsByclassId(id);
	}

}
