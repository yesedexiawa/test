package com.huidao.service;

import java.util.List;

import com.huidao.pojo.Students;

public interface StudentsService {
    /*
     * 根据ID删除学生
     */
    boolean deleteByPrimaryKey(Long stuId);

   /*
    * 新建一个学生
    */
    boolean insert(Students record);

   /*
    * 根据ID查询学生
    */
    Students selectByPrimaryKey(Long stuId);

    /*
     * 根据ID更新学生
     */
    boolean updateByPrimaryKey(Students record);
    
    /*
     * 获得所有学生
     */
    List<Students> selectAllStudents();
    
    List<Students> selectStudentsByclassId(Long id);
}