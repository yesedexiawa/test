package com.huidao.util;

import java.util.List;

public class Page<T> {
	private int pagenum;//当前页
	private long totalRecords;//总记录数
	private int pagesize=3;//每页显示数
	private int totalPages;//总页数
	private String url;//显示请求路径

	
	private  List<T> list;//页面需要显示的数据
	
	private int start;//给后台的数据
	private int end;
	
	
	
	
	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	//这个类需要pagenum,totalRecords但是自己没有
	public Page(int pagenum,long totalRecords){
		this.pagenum=pagenum;
		this.totalRecords=totalRecords;
		totalPages=(int) (totalRecords%pagesize==0?totalRecords/pagesize:totalRecords/pagesize+1);
		
		this.start=(pagenum-1)*pagesize;
		this.end=pagesize;
	}
	

	public int getStart() {
		return start;
	}


	public void setStart(int start) {
		this.start = start;
	}


	public int getEnd() {
		return end;
	}


	public void setEnd(int end) {
		this.end = end;
	}


	public int getPagenum() {
		return pagenum;
	}


	public void setPagenum(int pagenum) {
		this.pagenum = pagenum;
	}


	public long getTotalRecords() {
		return totalRecords;
	}


	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}


	public int getPagesize() {
		return pagesize;
	}


	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}


	public int getTotalPages() {
		return totalPages;
	}


	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}


	public List<T> getList() {
		return list;
	}


	public void setList(List<T> list) {
		this.list = list;
	}


	@Override
	public String toString() {
		return "Page [pagenum=" + pagenum + ", totalRecords=" + totalRecords
				+ ", pagesize=" + pagesize + ", totalPages=" + totalPages
				+ ", url=" + url + ", list=" + list + ", start=" + start
				+ ", end=" + end + "]";
	}
	
}
