package com.huidao.mapper;

import com.huidao.pojo.Classes;

public interface ClassesMapper {
  /*
   * 根据ID删除班级
   */
    int deleteByPrimaryKey(Long clsId);

   /*
    * 添加一个新的班级
    */
    int insert(Classes record);


   /*
    * 根据ID查找班级
    */
    Classes selectByPrimaryKey(Long clsId);



    /*
     * 根据ID更新班级
     */
    int updateByPrimaryKey(Classes record);
}