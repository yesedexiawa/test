package com.huidao.mapper;

import java.util.List;

import com.huidao.pojo.Students;

public interface StudentsMapper {
    /*
     * 根据ID删除学生
     */
    int deleteByPrimaryKey(Long stuId);

   /*
    * 新建一个学生
    */
    int insert(Students record);

   /*
    * 根据ID查询学生
    */
    Students selectByPrimaryKey(Long stuId);

    /*
     * 根据ID更新学生
     */
    int updateByPrimaryKey(Students record);
    
    /*
     * 获得所有学生
     */
    List<Students> selectAllStudents();
    /*
     * 根据班级ID获取学生
     */
    List<Students> selectStudentsByclassId(Long id);
}