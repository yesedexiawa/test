package com.huidao.contronller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

import javax.validation.Valid;

import com.huidao.pojo.Students;
import com.huidao.service.StudentsService;
@Controller
public class StudentsContronller {
		@Autowired
		private StudentsService studentsService;
		
		
		public void setStudentsService(StudentsService studentsService) {
			this.studentsService = studentsService;
		}


		@RequestMapping(value={"/"},method=RequestMethod.GET)
		public String getAllStudent(Model model){
			List<Students> list=studentsService.selectAllStudents();
			model.addAttribute("list",list);
			return "studentslist";
		}
		@RequestMapping(value="deleteUser/{id}")
		public String deleteOne(@PathVariable("id") Long id){
			studentsService.deleteByPrimaryKey(id);
			return "redirect:/";
		}
		@RequestMapping(value="seeOne/{id}")
		public String seeOne(@PathVariable("id") Long id,Model model){
			Students stu=studentsService.selectByPrimaryKey(id);
			model.addAttribute("student",stu);
			return "seeone";
		}
		@RequestMapping("toUpdate/{id}")
		public String toUpdate(@PathVariable("id") Long id,Model model,@ModelAttribute Students students){
			Students stu=studentsService.selectByPrimaryKey(id);
			model.addAttribute("students",stu);
			return "updatestudent";
		}
		@RequestMapping(value="/updateStudent",method=RequestMethod.POST)
		public String updateStudent(@Valid Students students,BindingResult br){
			System.out.println(br.getErrorCount());
			if(br.hasErrors()){
				return "updatestudent";
			}
			studentsService.updateByPrimaryKey(students);
			return "redirect:/";
		}
		@RequestMapping("selecStudentsByClassid/{id}")
		public String selecStudentsByClassid(@PathVariable("id") Long id,Model model){
			List<Students> list=studentsService.selectStudentsByclassId(id);
			model.addAttribute("list",list);
			return "seeclass";
		}
		@RequestMapping("toadd")
		public String toadd(@ModelAttribute Students students){
			return "addstudents";
		}
		@RequestMapping("addstudents")
		public String addStudents(Students students){
			studentsService.insert(students);
			return "redirect:/";
		}
}	
