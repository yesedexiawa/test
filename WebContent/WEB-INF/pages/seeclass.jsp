<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%	String basePath=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
						//              ://localhots :8080 :  S3Test03/
			// 真实地址 http://localhost:8080/HDMarket
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<base href="<%=basePath%>">
</head>
<body>	
	<div align="center">
		<table  border="1" cellspacing="0" width="80%">
			<tr><th>编号</th><th>姓名</th><th>年龄</th><th>班级</th><th>操作</th></tr>
			<c:forEach items="${list}" var="students">
			<tr>
			<th>${students.stuId}</th>
			<th>${students.name}</th>
			<th>${students.age}</th>
			<th>${students.classes.classname}</th>
			<th><a href="deleteUser/${students.stuId}">删除</a>&nbsp;<a href="toUpdate/${students.stuId}">修改</a>&nbsp;<a href="seeOne/${students.stuId}">详情</a>&nbsp;</th>
			</tr>
			</c:forEach>
		</table>
			<a href="javascript:history.back()">返回</a>
	</div>
</body>
</html>