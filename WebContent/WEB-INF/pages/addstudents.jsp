<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%	String basePath=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
						//              ://localhots :8080 :  S3Test03/
			// 真实地址 http://localhost:8080/HDMarket
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<base href="<%=basePath%>">
</head>
<body>
	<div>
		<sf:form action="addstudents" method="post" modelAttribute="students">
			<table>
				<tr>
					<td>姓名</td>
					<td><sf:input path="name"  type="text" /><sf:errors path="name"/></td>
				</tr>
				<tr>
					<td>年龄</td>
					<td><sf:input path="age"  type="number" /><sf:errors path="age"/></td>
				</tr>
				<tr>
					<td>班级</td>
					<td><sf:select path="classId">
					 	   <option value="0">--请选择--</option>
						   <option value="1">高一一班</option>
						   <option value="2">高一二班</option>
						   <option value="3">高二二班</option>
						   <option value="4">高三三班</option>
	        			 </sf:select>
	        		</td>
				</tr>
			</table>
					<input type="submit" value="添加">
		</sf:form>
	</div>
</body>
</html>